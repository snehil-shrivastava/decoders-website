const { gql } = require("apollo-server-core");

module.exports = gql`
type Query {
  queryContestWinners(contestName: String) : WinnersResult
  listMember(role: String) : [MemberGroup]
  me(id: String) : MemberProfile
}

type Mutation {
    createMember(
    id: String,
    password: String,
    name : String,
    year: String,
    photo: Upload,
    role: String,
    socialLinks : [SocialLinksFeild]) : Member
    removeMember(id: String) : String
    login(username: String, password: String): Boolean
    logout: Boolean
}

enum DataType {
    Integer
}

type FieldSet {
  label: String,
  dataType: DataType
}

type WinnersResultFeild{
  rank: Int,
  name: String,
  score: Float,
  country: String,
}

type WinnersResult {
  winners: [WinnersResultFeild]
}

type MemberProfile implements Member{
  id: String,
  name : String,
  year: String,
  photourl: String,
  socialLinks : [SocialLinks],
  aboutme_short: String,
  designation: String,
  aboutme_long: String
}

type MemberGroup {
  year: String
  members: [IMember]
}

type IMember implements Member{
  id: String,
  name : String,
  year: String,
  photourl: String,
  socialLinks : [SocialLinks],
  aboutme_short: String,
}

interface Member {
  id: String,
  name : String,
  year: String,
  photourl: String,
  socialLinks : [SocialLinks],
  
  aboutme_short: String
}

enum Platform {
  facebook, linkedin, email, twitter, instagram, reddit
}

input SocialLinksFeild {
  link: String,
  platform: Platform
}

type SocialLinks {
  link: String,
  platform: Platform
}

`