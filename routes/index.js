const { query, getMemberByUsername, registerMember, removeMember, getMemberProfile } = require('../controllers/DbController.js');
const { ApolloServer } = require('apollo-server-express');
const typeDefs = require('./type-defs.js');
const { requiresLogin, adminsOnly } = require('../controllers/AuthorizationController.js');
const { getSocialLinksFromRowData, handleUploadFile, handleDeleteFile } = require('../utils');
const { Forbidden } = require('http-errors');
var bcrypt = require('bcryptjs');

var resolvers = {
  Query: {
    queryContestWinners: async function (args) {
      const { contestName } = args
      let items = []
      await query('select * from ' + contestName + ';').then(async rows => {
        await rows.forEach(e => {
          items.push({ rank: e._rank, name: e.name, score: e.score, country: e.country })
        });
      })
      return { "winners": items }
    },

    listMember: async (_, { role }, context) => {
      const toRet = []
      let items = []
      const promise = role == null ? query('select a.*, b.aboutme_short from member a, member_profile b where a.id = b.id order by a.year asc;') : query('select * from member where role = ?;', [role])
      // TODO - fix for role
      await promise.then(async rows => {
        await rows.forEach(async e => {
          if(items[0] != undefined && items[0].year != e.year ){
            toRet.push({
              year: items[0].year,
              members: [...items]
            })
            items.length = 0
          }
          items.push({
            id: e.id,
            name: e.name,
            year: e.year,
            photourl: e.photourl,
            socialLinks: await context.getSocialLinksFromRowData(e),
            aboutme_short: e.aboutme_short
          })
        });
      })
      toRet.push({
        year: items[0].year,
        members: [...items]
      })
      return toRet
    },

    me: requiresLogin(async (parent, args, { user }) => {
      let profile = await getMemberProfile(user.id)
      if (profile == undefined)
        profile = {
          aboutme_short: "",
          designation: "",
          aboutme_long: ""
        }
      return { ...user , ...profile}
    })
  },


  Mutation: {
    createMember: async (_, { id, password, name, year, photo, socialLinks, role }, context) => {
      const doesMemberExits = await getMemberByUsername(id) != undefined
      if (doesMemberExits)
        return Error('Member already exists')
      const photourl = await context.handleUploadFile(photo)

      return await registerMember({ id, password, name, year, photourl, socialLinks, role })
        .catch((error) => {
          console.error(error)
        }).then(async rows => {
          return { id, name, year, photourl, socialLinks }
        })
    },
    login: async (_, { username, password }, context) => { 
      if(context.user != undefined){
        return false
      }
      const user = {... await getMemberByUsername(username)}
      if (!user.password) {
        return false
      }
      
      const valid = await bcrypt.compare(password, user.password)

      if (!valid) {
        return false
      }
      delete user.password;
      context.login(user, (e) => {console.error(e)})
      return true
    },
    logout: requiresLogin(async (parent, args, { logout }) => {
      logout()
      return true
    }),
    removeMember: adminsOnly(async (parent, args, { user, logout }) => {
      const { id } = args
      const member = await getMemberByUsername(id)
      if (member == undefined)
        return Error('No member with the username found')
      handleDeleteFile(member.photourl)
      await removeMember(id)
      if (id == user.id)
        logout()
      return `sucessfully removed ${member.id}`
    })
  }
};


module.exports = new ApolloServer({
  cors: true, typeDefs, resolvers,
  context: ({ req }) => {
    return {
      user: req.user || null,
      getUser: () => req.user,
      logout: () => req.logout(),
      login: req.logIn,
      getSocialLinksFromRowData,
      handleUploadFile
    }
  },
});