const createError = require('http-errors');
const express = require('express');
const path = require('path');
const join = require('path').join;
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const server = require('./routes/index.js');
const cors = require('cors')
const passport = require('./passport')
const session = require('express-session')
const { sql } = require('.//controllers/DbController')

// Origins for cors based on production and development
const defaultEnv = 'development'
const env = process.env.NODE_ENV || defaultEnv;
const allowedOrigins = env == defaultEnv ? [`http://localhost:${process.env.PORT}`, 'http://localhost:8000',
`http://192.168.29.181:${process.env.PORT}`, 'electron://firecamp'] : ['http://demo.decoderssit.com'];

console.info("allowed origins are :- %o", allowedOrigins)

const application = express();
application.use(cookieParser());
application.use(express.urlencoded({ extended: false }));
application.use(express.static(join(__dirname, 'public')));
application.set('views', join(__dirname, 'views'));
application.set('view engine', 'pug');
application.use(express.json());

application.use(cors({
  credentials: true,
  origin: function (origin, callback) {
    if (!origin) return callback(null, true);
    if (allowedOrigins.indexOf(origin) === -1) {
      console.error(origin)
      var msg = 'The CORS policy for this site does not ' +
        'allow access from the specified Origin.';
      return callback(new Error(msg), false);
    }
    return callback(null, true);
  }
}))
var MySQLStore = require('express-mysql-session')(session);

var sessionStore = new MySQLStore({}, sql);

application.use(session({
  key: process.env.COOKIE,
  secret: process.env.SESSION_SECRECT,
  resave: false,
  saveUninitialized: false,
  store: sessionStore
}))

application.use(passport.initialize())
application.use(passport.session())
application.use(logger('dev'));

application.use('/auth/login', function (req, res, next) {
  res.render('login.pug')
  next()
});

application.post('/login', function (req, res, next) {
  res.header('Access-Control-Allow-Origin', "*");
  res.header('Access-Control-Allow-Headers', "*");
  next()
}, passport.authenticate('local', {
  successRedirect: '/graphql',
  failureRedirect: '/failure',
  failureFlash: false,
  session: true,
}));



server.applyMiddleware({ app: application, cors: false })
// catch 404 and forward to error handler
application.use(function (req, res, next) {
  next(createError(404));
});

// error handler
application.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = application;
