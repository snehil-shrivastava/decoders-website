var passport = require('passport')
var LocalStrategy = require('passport-local').Strategy;
var { getMemberByUsername } = require('./controllers/DbController')
var bcrypt = require('bcryptjs');

passport.use(
  new LocalStrategy({
    session: true,
    passReqToCallback: true,
    usernameField: 'username',
    passwordField: 'password'
  },
  async function (req, username, password, done) {
      const user = {... await getMemberByUsername(username)}
      if (!user.password) {
        return done(new Error('No user with that username.'), null)
      }
      
      const valid = await bcrypt.compare(password, user.password)

      if (!valid) {
        return done(new Error('Incorrect password'), null)
      }
      delete user.password;
      return done(null, user);
    })
);

passport.serializeUser(function (user, done) {
  done(null, user.id);
});

passport.deserializeUser(async function (id, done) {
  const user = {... await getMemberByUsername(id)}
  delete user.password;
  done(null, user);
});


module.exports = passport;