const {AuthenticationError} = require('apollo-server-express')

const requiresRole = role => resolver => {
    return (parent, args, context, info) => {
      if (context.user && (!role || context.user.role === role)) {
        return resolver(parent, args, context, info)
      } else {
        throw new AuthenticationError('Unauthorized')
      }
    }
  }


const membersOnly = requiresRole('MEMBER')
const adminsOnly = requiresRole('ADMIN')
const requiresLogin = requiresRole(null)

module.exports = {
  membersOnly,
  adminsOnly,
  requiresLogin
}