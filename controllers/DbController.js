const mysql = require("mysql");
var bcrypt = require('bcryptjs');

console.debug("trying to acesss the database - %o", process.env.DATABASE)
console.debug("host - %o", process.env.HOST)
console.debug("password - %o", process.env.PASSWORD)
console.debug("user - %o", process.env.USERX)

const sql = mysql.createConnection({
  host: process.env.HOST,
  user: process.env.USERX,
  password: process.env.PASSWORD,
  database: process.env.DATABASE
});

function connect() {
  sql.connect(function (err) {
    if (err) {
      console.error(err)
      console.warn('Error connecting to Db');
      return;
    }
    console.info('sucessful connection established with mysql database');
  });
}

connect()

function query(smt, args) {
  return new Promise((resolve, reject) => {
    sql.query(smt, args, (err, rows) => {
      if (err)
        return reject(err);
      resolve(rows);
    });
  });
}

function close() {
  return new Promise((resolve, reject) => {
    sql.end(err => {
      if (err)
        return reject(err);
      resolve();
    });
  });
}

// members info 

// feild year, name, photoUrl, socialLinks, id

const registerMember = async (member) => {
  const { id, password, year, name, photourl, socialLinks } = member
  const modLinks = {}
  await socialLinks.forEach((x) => {
    modLinks[x.platform] = x.link
  })
  const epass = await bcrypt.hash(password, 8);
  const { facebook, linkedin, email, twitter, instagram, reddit } = modLinks
  return query("INSERT INTO member (id, name, year, password, photourl, facebook, linkedin, email, twitter, instagram, reddit) VALUES (?,?,?,?,?,?,?,?,?,?,?)", [id, name, year, epass, photourl, facebook, linkedin, email, twitter, instagram, reddit])
}

const updateMember = async (member) => {
  const { id, year, name, photourl, socialLinks } = member
  const { facebook, linkedin, email, twitter, instagram, reddit } = modLinks
  return query("UPDATE member SET name = ?, year = ?, photourl = ?, facebook = ?, linkedin = ?, email = ?, twitter = ?, instagram = ?, reddit = ? WHERE id  = ?; ", [id, name, year, photourl, facebook, linkedin, email, twitter, instagram, reddit])
}

const removeMember = (id) => {
  return query("DELETE FROM `member` WHERE `id` = ?;", [id])
}

const getMemberByUsername = async (username) => {
  return await query('select * from member where id = ?', [username])
    .catch(e => {console.log(e); return e})
    .then(res => {
      return res[0]
    })
}

const getMemberProfile = async (id) => {
  return await query('select * from member_profile where id = ?', [id])
    .catch(e => {console.log(e); return e})
    .then(res => {
      return res[0]
    })

}

module.exports = {
  registerMember,
  updateMember,
  removeMember,
  getMemberByUsername,
  getMemberProfile,
  close,
  connect,
  query,
  sql
}