
const { v4: uuidv4 } = require('uuid');
const fs = require('fs');

async function getSocialLinksFromRowData(e) {
    const socialLinks = []
    const { facebook, linkedin, email, twitter, instagram, reddit } = e
    const links = { facebook, linkedin, email, twitter, instagram, reddit }
    await Promise.all(Object.keys(links).map(async function (key) {
        if (links[key] != null) {
            socialLinks.push({
                link: links[key],
                platform: key
            });
        }
    }));
    return socialLinks
}

async function handleUploadFile(upload) {
    const { createReadStream, filename, mimetype } = await upload;
    const stream = createReadStream();
    const id = uuidv4();
    const path = `${process.env.UPLOAD_DIR}/${id}-${filename}`;
    const file = { id, filename, mimetype, path };

    return await new Promise((resolve, reject) => {
        const writeStream = fs.createWriteStream(path);
        writeStream.on('finish', resolve);
        writeStream.on('error', (error) => {
            fs.unlink(path, () => {
                reject(error);
                console.error(error)
                throw error
            });
        });
        stream.on(
            'error', (error) => {
                writeStream.destroy(error)
                console.error(error)
                throw error
            }
        );
        stream.pipe(writeStream);
        console.info("file sucessfully uploaded to %o", path)
        resolve(path)
    });
}

const handleDeleteFile = (file) =>{
    return fs.unlink(file, (err) => {
        console.error("Utils.js - handleDeleteFile %o", err);
    });
}

module.exports = {
    getSocialLinksFromRowData,
    handleUploadFile,
    handleDeleteFile
}